PHP_SRCS = src/tmpl/php.gitlab-template.yml \
	src/deploy.sh \
	src/git-secret.sh \
	src/lando.sh \
	src/release.sh \
	src/vault.sh

make_php_gitlab_ci_tmpl: $(PHP_SRCS)
	./build.sh $(PHP_SRCS) > templates/php.gitlab-template.yml