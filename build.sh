#!/bin/bash

cat << EOF
# Auto generated gitlab-ci file. Don't edit this file directly, but use make to
# generate a new one. 
EOF

scripts() {
  local script
  echo ".scripts:"
  for script in $@ ; do
    echo "  - &${script//[^a-zA-Z_-]/_} |"
    IFS=$'\n'; while read -r line || [ -n "$line" ] ; do
      echo "    ${line}"
    done < "$script"
  done
}

template=$1; shift
scripts="$(scripts $@)"

# Awk treats backslashes as escapes and ampersands as backreferences, so we need
# to fix that (or use something else instead of awk if it leads to more issues).
scripts="${scripts//\\/\\\\}" # Translate \ -> \\
scripts="${scripts//&/\\\\&}" # Translate & -> \\&

awk -v r="${scripts}" '{gsub(/__SCRIPTS__/,r)}1' ${template}