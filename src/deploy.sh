# Deploy scripts

deploy_source_env() {
  if [ -f "${DEPLOY_ENV_FILE}" ] ; then
    set -a
    source "${DEPLOY_ENV_FILE}"
    set +a
  else
    echo 'No .env file to source'
  fi
}

deploy_install_sshkey() {
  mkdir -p ~/.ssh/ 
  echo "${DEPLOY_SSHKEY}" > ~/.ssh/id_rsa 
  echo "${DEPLOY_KNOWN_HOSTS}" > ~/.ssh/known_hosts
  echo "${DEPLOY_CONFIG}" > ~/.ssh/config
  chmod -R 600 ~/.ssh
}

deploy_patch_update() {
  export DEPLOY_SRC=${DEPLOY_SRC:-./}
  export DEPLOY_DEST=${DEPLOY_DEST:-${CI_PROJECT_PATH_SLUG:-/app/}}
  echo "Pushing files to ${DEPLOY_HOST}:${DEPLOY_DEST}"
  touch "${DEPLOY_EXCLUDE:-.deploy-exclude}"
  rsync -avz --delete --checksum -e ssh \
    --exclude-from="${DEPLOY_EXCLUDE:-.deploy-exclude}" \
    "${DEPLOY_SRC}" ${DEPLOY_HOST}:${DEPLOY_DEST} \
    | grep -E '^deleting|[^/]$'
}
