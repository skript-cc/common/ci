lando_install_composer_2() {
  php -r "copy('https://getcomposer.org/installer', '/tmp/composer-setup.php');"
  php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer --2
  php -r "unlink('/tmp/composer-setup.php');"
}