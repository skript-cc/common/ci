
git_secret_install() {
  if [ -d "${SECRETS_DIR}" ] && [ -n "${GPG_PRIVATE_KEY}" ] ; then
    # Install git-secret (https://git-secret.io/installation)
    apt-get update && apt-get install apt-transport-https gnupg2 -y
    sh -c "echo 'deb https://gitsecret.jfrog.io/artifactory/git-secret-deb git-secret main' >> /etc/apt/sources.list"
    wget -qO - 'https://gitsecret.jfrog.io/artifactory/api/gpg/key/public' | apt-key add -
    apt-get install -y git-secret
    # Import private key
    echo "$GPG_PRIVATE_KEY" | gpg --import -
  else
    echo 'No git secrets to reveal'
  fi
}

git_secret_delete_all_except_secrets_and_build() {
  mkdir tmpkeep
  if [ -d "${SECRETS_DIR}" ] && [ -n "${GPG_PRIVATE_KEY}" ] ; then
    git secret list | xargs -d '\n' -I{} mv "{}" "tmpkeep/{}" --
  fi
  find $(pwd) -type f \
    -not -path "$(realpath ./tmpkeep/)*"\
    -not -path "$(realpath ${BUILD_PROD_ARCHIVE})"\
    -not -path "$(realpath ${BUILD_DEV_ARCHIVE})"\
    -delete
  find tmpkeep -type f -print0 | xargs -0 -I{} mv "{}" . --
  rm -r tmpkeep
  find . -type d -empty -delete
}

git_secret_reveal() {
  if [ $(which git-secret) ] ; then
    git secret reveal
  else
    echo "Skip git secret reveal; git secret is not installed"
  fi
}