vault_source_env() {
  if [ -f "${PASSWORD_STORE_DIR}/${VAULT_ENVFILE}.gpg" ] && [ -n "${WEBMASTER_GPG_KEY}" ] ; then
    apt-get update && apt-get install pass -y
    echo "$WEBMASTER_GPG_KEY" | gpg --import -
    local dotenv=$(mktemp)
    pass show "${VAULT_ENVFILE}" > $dotenv
    source $dotenv
    rm $dotenv
  fi
}