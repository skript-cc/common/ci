release_create() { 
  curl \
    --request POST \
    --header "PRIVATE-TOKEN: ${CI_JOB_TOKEN}" \
    --form "file=${BUILD_PROD_ARCHIVE}"  \
    ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/uploads > prod_archive.json
  
  UPLOAD_PATH=$(cat prod_archive.json | php -r 'echo (json_decode(stream_get_contents(STDIN)))->url;')
  
  echo "Production build is uploaded to ${CI_PROJECT_URL}${UPLOAD_PATH}"
  
  curl \
    --request POST \
    --header 'Content-Type: application/json' \
    --header "Private-Token: ${CI_JOB_TOKEN}" \
    --data "$(release_json)" \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases"
}

release_json() {
cat <<EOF
{
  "name": "Release ${CI_COMMIT_REF_NAME}",
  "tag_name": "${CI_COMMIT_REF_NAME}",
  "description": "Release ${CI_COMMIT_REF_NAME}",
  "assets": {"links": [
    {"name": "Production build", "url": "${CI_PROJECT_URL}${UPLOAD_PATH}"}
  ]}
}
EOF
}