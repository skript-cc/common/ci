# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.3] - 2022-09-02
### Fixed
- Git secret install

## [0.2.2] - 2021-01-29
### Fixed
- Setup composer on test job

## [0.2.1] - 2021-01-29
### Fixed
- Setup composer on deploy job and fail harder on failing scripts

## [0.2.0] - 2021-01-29
### Added
- New composer setup check for 'composer auth' script before build and update jobs
- New USE_COMPOSER_2 gitlab-ci variable to control which composer version to use

### Changed
- Build and update jobs now use before_script to setup composer
- Use composer 2 by default for installs and updates
- Replace --optimize-autoloader flag with --classmap-authoritative on production installs

## [0.1.0] - 2020-12-08

Initial release

[Unreleased]: https://gitlab.com/skript-cc/common/ci/compare/0.2.3...master
[0.2.3]: https://gitlab.com/skript-cc/common/ci/compare/0.2.2...0.2.3
[0.2.2]: https://gitlab.com/skript-cc/common/ci/compare/0.2.1...0.2.2
[0.2.1]: https://gitlab.com/skript-cc/common/ci/compare/0.2.0...0.2.1
[0.2.0]: https://gitlab.com/skript-cc/common/ci/compare/0.1.1...0.2.0
[0.1.0]: https://gitlab.com/skript-cc/common/ci/-/tags/0.1.0